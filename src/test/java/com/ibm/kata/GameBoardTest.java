package com.ibm.kata;

import org.junit.Test;

import com.ibm.kata.enums.Player;
import com.ibm.kata.exceptions.SpaceTakenException;

public class GameBoardTest {

	@Test(expected = SpaceTakenException.class)
	public void testMarkingTakenSpace() throws SpaceTakenException {
		final GameBoard gameBoard = GameBoard.getInstance();

		gameBoard.setBoardSpace(Player.X, 2);
		gameBoard.setBoardSpace(Player.X, 2);
	}
}
