package com.ibm.kata;

import org.junit.Assert;
import org.junit.Test;

import com.ibm.kata.gamemodes.BotGame;

public class TicTacToeTest {

	@Test
	public void scenario1() {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();
		final BotGame botGame = new BotGame();
		botGame.setSleepDisabled(true);

		gameBoard.initializeGame();
		botGame.playGame(gameBoard, ruleChecker, "");

		String expectedResult = " | | " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += " | | " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += " | | " + System.lineSeparator();
		Assert.assertEquals(expectedResult, gameBoard.getGameBoardAsString());
		Assert.assertEquals(false, ruleChecker.isPlayerXWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isPlayerOWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isDraw(gameBoard));
		Assert.assertEquals(false, ruleChecker.isGameFinished(gameBoard));
	}

	@Test
	public void scenario2() {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();
		final BotGame botGame = new BotGame();
		botGame.setSleepDisabled(true);

		gameBoard.initializeGame();
		botGame.playGame(gameBoard, ruleChecker, "75431");

		String expectedResult = "X| | " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "X|O| " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "X| |O" + System.lineSeparator();
		Assert.assertEquals(expectedResult, gameBoard.getGameBoardAsString());
		Assert.assertEquals(true, ruleChecker.isPlayerXWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isPlayerOWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isDraw(gameBoard));
		Assert.assertEquals(true, ruleChecker.isGameFinished(gameBoard));
	}

	@Test
	public void scenario3() {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();
		final BotGame botGame = new BotGame();
		botGame.setSleepDisabled(true);

		gameBoard.initializeGame();
		botGame.playGame(gameBoard, ruleChecker, "749516");

		String expectedResult = "X| |X" + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "O|O|O" + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "X| | " + System.lineSeparator();
		Assert.assertEquals(expectedResult, gameBoard.getGameBoardAsString());
		Assert.assertEquals(false, ruleChecker.isPlayerXWinner(gameBoard));
		Assert.assertEquals(true, ruleChecker.isPlayerOWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isDraw(gameBoard));
		Assert.assertEquals(true, ruleChecker.isGameFinished(gameBoard));
	}

	@Test
	public void scenario4() {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();
		final BotGame botGame = new BotGame();
		botGame.setSleepDisabled(true);

		gameBoard.initializeGame();
		botGame.playGame(gameBoard, ruleChecker, "51347");

		String expectedResult = "X| | " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "O|X| " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "O| |X" + System.lineSeparator();
		Assert.assertEquals(expectedResult, gameBoard.getGameBoardAsString());
		Assert.assertEquals(true, ruleChecker.isPlayerXWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isPlayerOWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isDraw(gameBoard));
		Assert.assertEquals(true, ruleChecker.isGameFinished(gameBoard));
	}

	@Test
	public void scenario5() {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();
		final BotGame botGame = new BotGame();
		botGame.setSleepDisabled(true);

		gameBoard.initializeGame();
		botGame.playGame(gameBoard, ruleChecker, "152378946");

		String expectedResult = "X|O|X" + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "O|O|X" + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "X|X|O" + System.lineSeparator();
		Assert.assertEquals(expectedResult, gameBoard.getGameBoardAsString());
		Assert.assertEquals(false, ruleChecker.isPlayerXWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isPlayerOWinner(gameBoard));
		Assert.assertEquals(true, ruleChecker.isDraw(gameBoard));
		Assert.assertEquals(true, ruleChecker.isGameFinished(gameBoard));
	}

	@Test
	public void scenario6() {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();
		final BotGame botGame = new BotGame();
		botGame.setSleepDisabled(true);

		gameBoard.initializeGame();
		botGame.playGame(gameBoard, ruleChecker, "123456789");

		String expectedResult = "X| | " + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "O|X|O" + System.lineSeparator();
		expectedResult += "-+-+-" + System.lineSeparator();
		expectedResult += "X|O|X" + System.lineSeparator();
		Assert.assertEquals(expectedResult, gameBoard.getGameBoardAsString());
		Assert.assertEquals(true, ruleChecker.isPlayerXWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isPlayerOWinner(gameBoard));
		Assert.assertEquals(false, ruleChecker.isDraw(gameBoard));
		Assert.assertEquals(true, ruleChecker.isGameFinished(gameBoard));
	}

}
