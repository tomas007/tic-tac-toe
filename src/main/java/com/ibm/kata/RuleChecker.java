package com.ibm.kata;

import com.ibm.kata.enums.Player;

public class RuleChecker {

	private static RuleChecker instance = null;

	private RuleChecker() {
		super();
	}

	public static RuleChecker getInstance() {
		if (instance == null) {
			instance = new RuleChecker();
		}

		return instance;
	}

	public boolean isPlayerXWinner(final GameBoard gameBoard) {
		return this.isPlayerWinner(gameBoard, Player.X);
	}

	public boolean isPlayerOWinner(final GameBoard gameBoard) {
		return this.isPlayerWinner(gameBoard, Player.O);
	}

	public boolean isPlayerWinner(final GameBoard gameBoard, final Player player) {
		// horizontal
		if (gameBoard.getBoardSpace(1).equals(player) && gameBoard.getBoardSpace(2).equals(player)
				&& gameBoard.getBoardSpace(3).equals(player)
				|| gameBoard.getBoardSpace(4).equals(player) && gameBoard.getBoardSpace(5).equals(player)
						&& gameBoard.getBoardSpace(6).equals(player)
				|| gameBoard.getBoardSpace(7).equals(player) && gameBoard.getBoardSpace(8).equals(player)
						&& gameBoard.getBoardSpace(9).equals(player)) {
			return true;
		}

		// vertical
		if (gameBoard.getBoardSpace(1).equals(player) && gameBoard.getBoardSpace(4).equals(player)
				&& gameBoard.getBoardSpace(7).equals(player)
				|| gameBoard.getBoardSpace(2).equals(player) && gameBoard.getBoardSpace(5).equals(player)
						&& gameBoard.getBoardSpace(8).equals(player)
				|| gameBoard.getBoardSpace(3).equals(player) && gameBoard.getBoardSpace(6).equals(player)
						&& gameBoard.getBoardSpace(9).equals(player)) {
			return true;
		}

		// cross
		if (gameBoard.getBoardSpace(1).equals(player) && gameBoard.getBoardSpace(5).equals(player)
				&& gameBoard.getBoardSpace(9).equals(player)
				|| gameBoard.getBoardSpace(3).equals(player) && gameBoard.getBoardSpace(5).equals(player)
						&& gameBoard.getBoardSpace(7).equals(player)) {
			return true;
		}

		return false;
	}

	public boolean isDraw(final GameBoard gameBoard) {
		boolean isEmptySpace = false;
		for (int i = 1; i <= 9; i++) {
			if (gameBoard.getBoardSpace(i).equals(Player.EMPTY)) {
				isEmptySpace = true;
			}
		}

		return !this.isPlayerXWinner(gameBoard) && !this.isPlayerOWinner(gameBoard) && !isEmptySpace;
	}

	public boolean isGameFinished(final GameBoard gameBoard) {
		return this.isPlayerXWinner(gameBoard) || this.isPlayerOWinner(gameBoard) || this.isDraw(gameBoard);
	}

	public void printResult(final GameBoard gameBoard) {
		if (this.isPlayerXWinner(gameBoard)) {
			System.out.println("PLAYER X WON!");
		} else if (this.isPlayerOWinner(gameBoard)) {
			System.out.println("PLAYER O WON!");
		} else if (this.isDraw(gameBoard)) {
			System.out.println("GAME ENDS WITH A DRAW!");
		} else {
			System.out.println("Game ends because of no input!");
		}
	}
}
