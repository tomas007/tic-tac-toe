package com.ibm.kata;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.ibm.kata.exceptions.SpaceTakenException;
import com.ibm.kata.gamemodes.BotGame;
import com.ibm.kata.gamemodes.InteractiveGame;

public class TicTacToe {

	private static Options options = new Options();;

	public static void main(final String[] args) throws SpaceTakenException {
		final GameBoard gameBoard = GameBoard.getInstance();
		final RuleChecker ruleChecker = RuleChecker.getInstance();

		final CommandLine cmd = parseArguments(args);
		validateArguments(cmd);

		if (Mode.BOT.toString().equalsIgnoreCase(cmd.getOptionValue("m"))) {
			final BotGame botGame = new BotGame();
			botGame.playGame(gameBoard, ruleChecker);
		} else if (Mode.PRESCRIPTED.toString().equalsIgnoreCase(cmd.getOptionValue("m"))) {
			final BotGame botGame = new BotGame();
			botGame.playGame(gameBoard, ruleChecker, cmd.getOptionValue("v"));
		} else {
			final InteractiveGame interactiveGame = new InteractiveGame();
			interactiveGame.playGame(gameBoard, ruleChecker);
		}
	}

	private static CommandLine parseArguments(final String[] args) {
		options.addOption("m", "mode", true, "defines mode of the game (" + Mode.INTERACTIVE.toString().toLowerCase()
				+ ", " + Mode.BOT.toString().toLowerCase() + ", " + Mode.PRESCRIPTED.toString().toLowerCase() + ")");
		options.addOption("v", "moves", true,
				"used in mode \"prescripted\". Defines moves that are done in game. It's "
						+ "0 to 9 digit number - each digit is one "
						+ "move). For example digit 1 means that player will place "
						+ "his mark on the bottom left space on the game board. First digit is the first move of the game, "
						+ "second digit is second move, and so on.");
		options.addOption("h", "help", false, "prints this message");

		final CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args);
		} catch (final ParseException e) {
			System.err.println("Parsing of arguments failed. " + e.getMessage());
			printHelp(options);
			System.exit(1);
		}
		return cmd;
	}

	private static void printHelp(final Options options) {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("tic-tac-toe", options);
		System.out.println();
		System.out.println("Examples: ");
		System.out.println("tic-tac-toe");
		System.out.println("tic-tac-toe -m=bot");
		System.out.println("tic-tac-toe -m=prescripted -v=123456789");
		System.out.println("tic-tac-toe -m=prescripted -v=759614");
	}

	private static void validateArguments(final CommandLine cmd) {
		if (cmd.hasOption("m") && cmd.getOptionValue("m") == null) {
			System.err.println("Option --mode requires an argument. ");
			printHelp(options);
			System.exit(1);
		} else if (cmd.hasOption("m") && !cmd.getOptionValue("m").equalsIgnoreCase(Mode.INTERACTIVE.toString())
				&& !cmd.getOptionValue("m").equalsIgnoreCase(Mode.BOT.toString())
				&& !cmd.getOptionValue("m").equalsIgnoreCase(Mode.PRESCRIPTED.toString())) {
			System.err.println("Invalid argument in option --mode. ");
			printHelp(options);
			System.exit(1);
		} else if (cmd.hasOption("m") && cmd.getOptionValue("m").equalsIgnoreCase(Mode.PRESCRIPTED.toString())
				&& cmd.getOptionValue("v") == null) {
			System.err.println("Missing prescripted moves. Please use option -v with an argument. ");
			printHelp(options);
			System.exit(1);
		} else if (cmd.hasOption("h")) {
			printHelp(options);
			System.exit(0);
		}

	}

	private enum Mode {
		INTERACTIVE, BOT, PRESCRIPTED
	}
}
