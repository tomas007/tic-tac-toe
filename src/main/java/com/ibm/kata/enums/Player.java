package com.ibm.kata.enums;

public enum Player {
	X("X"),
	O("O"),
	EMPTY(" ");
	
	private String mark;
	
	Player(String mark) {
		this.mark = mark;
	}
	
	public String getMark() {
		return this.mark;
	}
}
