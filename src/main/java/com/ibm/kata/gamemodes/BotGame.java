package com.ibm.kata.gamemodes;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import com.ibm.kata.GameBoard;
import com.ibm.kata.RuleChecker;
import com.ibm.kata.enums.Player;
import com.ibm.kata.exceptions.SpaceTakenException;

public class BotGame extends Game {

	private boolean isSleepDisabled = false;

	@Override
	public void playGame(final GameBoard gameBoard, final RuleChecker ruleChecker) {
		final int[] movesAr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		this.shuffleArray(movesAr);

		this.playGame(gameBoard, ruleChecker, movesAr);
	}

	public void playGame(final GameBoard gameBoard, final RuleChecker ruleChecker, final String moves) {
		final int[] movesAr = new int[9];

		for (int i = 0; i < 9; i++) {
			if (i >= moves.length()) {
				movesAr[i] = 0;
			} else if (Character.isDigit(moves.charAt(i))) {
				movesAr[i] = Integer.parseInt(String.valueOf(moves.charAt(i)));
			} else {
				System.out.println("Moves contains an invalid digit.");
				return;
			}
		}

		this.playGame(gameBoard, ruleChecker, movesAr);
	}

	private void playGame(final GameBoard gameBoard, final RuleChecker ruleChecker, final int[] moves) {
		System.out.println("Game Board Creation...");
		gameBoard.printGameBoard();
		System.out.println("Board Created.");

		Player currentPlayer = Player.X;
		System.out.println("The game will start with Player " + currentPlayer.getMark());

		for (final int move : moves) {
			if (ruleChecker.isGameFinished(gameBoard) || move == 0) {
				break;
			}

			this.sleepFor2Seconds();

			System.out.println("Player " + currentPlayer.getMark() + ": ");

			try {
				gameBoard.setBoardSpace(currentPlayer, move);
				gameBoard.printGameBoard();

				currentPlayer = this.switchPlayers(currentPlayer);
			} catch (final SpaceTakenException e) {
				System.out.println("This space is already taken. Select different space. ");
			}
		}

		ruleChecker.printResult(gameBoard);
	}

	private void sleepFor2Seconds() {
		if (!this.isSleepDisabled) {
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (final InterruptedException e) {
				// continue
			}
		}
	}

	private void shuffleArray(final int[] array) {
		final Random rnd = ThreadLocalRandom.current();
		for (int i = array.length - 1; i > 0; i--) {
			final int randomIndex = rnd.nextInt(i + 1);

			final int a = array[randomIndex];
			array[randomIndex] = array[i];
			array[i] = a;
		}
	}

	public void setSleepDisabled(final boolean isSleepDisabled) {
		this.isSleepDisabled = isSleepDisabled;
	}
}
