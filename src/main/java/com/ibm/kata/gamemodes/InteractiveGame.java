package com.ibm.kata.gamemodes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.ibm.kata.GameBoard;
import com.ibm.kata.RuleChecker;
import com.ibm.kata.enums.Player;
import com.ibm.kata.exceptions.SpaceTakenException;

public class InteractiveGame extends Game {

	/**
	 * Interactive game reads input from standard input.
	 *
	 * @param gameBoard
	 * @param ruleChecker
	 */
	@Override
	public void playGame(final GameBoard gameBoard, final RuleChecker ruleChecker) {
		System.out.println("Game Board Creation...");
		gameBoard.printGameBoard();
		System.out.println("Board Created.");

		Player currentPlayer = Player.X;
		System.out.println("The game will start with Player " + currentPlayer.getMark());

		BufferedReader consoleReader = null;
		try {
			consoleReader = new BufferedReader(new InputStreamReader(System.in));

			while (!ruleChecker.isGameFinished(gameBoard)) {
				System.out.print("Player " + currentPlayer + ": Enter number of the space you want to mark (1-9): ");
				final String input = consoleReader.readLine();

				if (input.equals("exit")) {
					break;
				}

				currentPlayer = this.makeNextMove(gameBoard, currentPlayer, input);
			}

		} catch (final IOException e) {
			System.err.println("Fatal error occured. Exiting.");
			System.exit(1);
		} finally {
			if (consoleReader != null) {
				try {
					consoleReader.close();
				} catch (final IOException e) {
					// ignore. It's end of the game anyway.
				}
			}
		}

		ruleChecker.printResult(gameBoard);
	}

	private Player makeNextMove(final GameBoard gameBoard, Player currentPlayer, final String inputString) {
		if (this.isInputValid(inputString)) {
			try {
				final int input = Integer.parseInt(inputString);
				gameBoard.setBoardSpace(currentPlayer, input);
				gameBoard.printGameBoard();

				currentPlayer = this.switchPlayers(currentPlayer);
			} catch (final SpaceTakenException e) {
				System.out.println("This space is already taken. Select different space. ");
			}
		} else {
			System.out.println("Invalid input. Use number from 1 to 9. Or use \"exit\" to end the game.");
		}
		return currentPlayer;
	}

	private boolean isInputValid(final String input) {
		if (input != null && (input.matches("^[1-9]$") || input.equals("exit"))) {
			return true;
		} else {
			return false;
		}
	}

}
