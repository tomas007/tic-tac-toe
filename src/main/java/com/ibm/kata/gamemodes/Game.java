package com.ibm.kata.gamemodes;

import com.ibm.kata.GameBoard;
import com.ibm.kata.RuleChecker;
import com.ibm.kata.enums.Player;

public abstract class Game {

	public abstract void playGame(final GameBoard gameBoard, final RuleChecker ruleChecker);

	protected Player switchPlayers(Player currentPlayer) {
		if (currentPlayer == Player.X) {
			currentPlayer = Player.O;
		} else {
			currentPlayer = Player.X;
		}
		return currentPlayer;
	}
}
