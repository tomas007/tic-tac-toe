package com.ibm.kata.exceptions;

public class SpaceTakenException extends Exception {

	public SpaceTakenException() {
		super();
	}

	public SpaceTakenException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SpaceTakenException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public SpaceTakenException(final String message) {
		super(message);
	}

	public SpaceTakenException(final Throwable cause) {
		super(cause);
	}

}
