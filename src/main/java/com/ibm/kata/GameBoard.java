package com.ibm.kata;

import com.ibm.kata.enums.Player;
import com.ibm.kata.exceptions.SpaceTakenException;

public class GameBoard {

	private static GameBoard instance = null;

	private final Player[] boardSpaces = new Player[9];

	private GameBoard() {
		super();
		this.initializeGame();
	}

	public static GameBoard getInstance() {
		if (instance == null) {
			instance = new GameBoard();
		}
		return instance;
	}

	/**
	 * Cleans game board for fresh game.
	 */
	public void initializeGame() {
		for (int i = 0; i < 9; i++) {
			this.boardSpaces[i] = Player.EMPTY;
		}
	}

	/**
	 * Set mark on given space in game board.
	 *
	 * @param playerMark
	 *            mark
	 * @param index
	 *            index of space (from 1 to 9)
	 * @throws SpaceTakenException
	 */
	public void setBoardSpace(final Player playerMark, final int index) throws SpaceTakenException {
		if (index < 1 || index > 9) {
			throw new IllegalArgumentException("Invalid value in index argument. Expected values - from 1 to 9.");
		}

		if (this.boardSpaces[index - 1] != Player.EMPTY) {
			throw new SpaceTakenException("This space is already taken.");
		}

		this.boardSpaces[index - 1] = playerMark;
	}

	/**
	 * Prints game board to given standard output.
	 */
	public void printGameBoard() {
		final String gameBoardString = this.getGameBoardAsString();
		System.out.print(gameBoardString);
	}

	/**
	 * @return String representation of the game board state.
	 */
	public String getGameBoardAsString() {
		final StringBuilder gameBoardSB = new StringBuilder();

		this.appendGameBoardLine(gameBoardSB, 3);
		this.appendSeparatorLine(gameBoardSB);
		this.appendGameBoardLine(gameBoardSB, 2);
		this.appendSeparatorLine(gameBoardSB);
		this.appendGameBoardLine(gameBoardSB, 1);

		return gameBoardSB.toString();
	}

	private void appendSeparatorLine(final StringBuilder textBoard) {
		textBoard.append("-+-+-" + System.lineSeparator());
	}

	private void appendGameBoardLine(final StringBuilder textBoard, final int lineNumber) {
		if (lineNumber < 1 || lineNumber > 3) {
			throw new IllegalArgumentException("Invalid value in lineNumber argument. Expected values - from 1 to 3.");
		}

		final int boardSpaceIndex = (lineNumber - 1) * 3;
		textBoard.append(this.boardSpaces[boardSpaceIndex].getMark() + "|");
		textBoard.append(this.boardSpaces[boardSpaceIndex + 1].getMark() + "|");
		textBoard.append(this.boardSpaces[boardSpaceIndex + 2].getMark() + System.lineSeparator());
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Singleton class does not support cloning.");
	}

	public Player getBoardSpace(final int index) {
		if (index < 1 || index > 9) {
			throw new IllegalArgumentException("Invalid index: " + index);
		}

		return this.boardSpaces[index - 1];
	}

}
